/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 0;        /* border pixel of windows */
static const unsigned int gappx     = 6;       /* gap pixel between windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayonleft = 0;   	/* 0: systray in the right corner, >0: systray on left of status text */
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray        = 1;     /* 0 means no systray */
static const int showbar            = 1;     /* 0 means no bar */
static const int topbar             = 1;     /* 0 means bottom bar */
/* Fonts */
/* Source Han Sans CN - 汉字; WenQuanYi Zen Hei - 八卦符号; Hasklug Nerd Font - nerd icons; */
static const char *fonts[]          = { "Hasklug Nerd Font:size=12","WenQuanYi Zen Hei:size=12" };
static const char dmenufont[]       = "FantasqueSansMono Nerd Font Mono:size=12";
/* Color and Theme */
/* orig dark color */
static const char col_gray1[]       = "#222222";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#bbbbbb";
static const char col_gray4[]       = "#eeeeee";
static const char col_cyan1[]       = "#005577";
/* orig light color */
static const char col_gray01[]      = "#cccccc";
static const char col_gray02[]      = "#eeeeee";
static const char col_gray03[]      = "#000000";
static const char col_gray04[]      = "#eeeeee";
static const char col_cyan01[]      = "#0066ff";
/* solarized colors http://ethanschoonover.com/solarized */
static const char s_base03[]        = "#002b36";
static const char s_base02[]        = "#073642";
static const char s_base01[]        = "#586e75";
static const char s_base00[]        = "#657b83";
static const char s_base0[]         = "#839496";
static const char s_base1[]         = "#93a1a1";
static const char s_base2[]         = "#eee8d5";
static const char s_base3[]         = "#fdf6e3";
/* dracula corlors https://draculatheme.com */
static const char col_bg[]          = "#282a36";
static const char col_sel[]         = "#44475a";
static const char col_fg[]          = "#f8f8f2";
static const char col_comment[]     = "#6272a4";
static const char col_red[]         = "#ff5555"; // red
static const char col_green[]       = "#50fa7b"; // green
static const char col_yellow[]      = "#f1fa8c"; // yellow
static const char col_orange[]      = "#ffb86c"; // orange
static const char col_purple[]      = "#bd93f9"; // blue
static const char col_pink[]        = "#ff79c6"; // magenta
static const char col_cyan[]        = "#8be9fd"; // cyan

static const char *colors[][3]      = {
	/*               fg         bg         border   */
	{ s_base0, s_base03, s_base2 },      /* SchemeNorm solarized dark */
	{ s_base0, s_base02, s_base2 },      /* SchemeSel solarizeddark */
	{ s_base00, s_base3, s_base02 },     /* SchemeNorm solarizedlight */
	{ s_base00, s_base2, s_base02},      /* SchemeSel solarizedlight */
	{ col_gray3, col_gray1, col_gray2 }, /* SchemeNorm orig dark */
	{ col_gray4, col_cyan1,  col_cyan1 }, /* SchemeSel orig dark */
	{ col_gray03, col_gray01, col_gray02 }, /* SchemeNorm orig light */
	{ col_gray04, col_cyan01,  col_cyan01 }, /* SchemeSel orig light */
    { col_comment, col_bg, col_fg },     /* SchemeNorm dracula */
    { col_purple, col_sel, col_fg },     /* SchemeSel dracula */
};
static const XPoint stickyicon[]    = { {0,0}, {4,0}, {4,8}, {2,6}, {0,8}, {0,0} }; /* represents the icon as an array of vertices */
static const XPoint stickyiconbb    = {4,8};	/* defines the bottom right corner of the polygon's bounding box (speeds up scaling) */

/* cool autostart */
static const char *const autostart[] = {
	"slstatus", NULL,
	//"nitrogen", "--restore", NULL,
	"/home/wonux/.fehbg", "&", NULL,
	"picom", "-b", NULL,
	"fcitx", "&", NULL,
	"nm-applet", "&", NULL,
	"plank", "&", NULL,
	//"fcitx5", "&", NULL,
	//"nutstore", "&", NULL,
	"easystroke", "&", NULL,
	"greenclip", "daemon", NULL,
	//"alacritty", NULL,
	NULL /* terminate */
};

/* tagging */
//static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };
//static const char *tags[] = { ">_☯", "☰乾", "☷坤", "☳震", "☴巽", "☵坎", "☲离", "☶艮", "☱兑" };
//static const char *tags[] = { ">_", "☰", "☷", "☳", "☴", "☵", "☲", "☶", "☱" };
//static const char *tags[] = { "ﭾ", "乾", "坤", "震", "巽", "坎", "离", "艮", "兑" };
static const char *tags[] = { "", "", "", "", "", "", "", "", "" };
//static const char *tagsalt[] = { "", "", "", "", "", "", "", "", "" };
static const char *tagsalt[] = { "󰎤", "󰎧", "󰎪", "󰎭", "󰎱", "󰎳", "󰎶", "󰎹", "󰎼" };
//static const char *tagsalt[] = { "", "", "", "", "", "", "", "", "" };
static const int momentaryalttags = 0; /* 1 means alttags will show only when key is held down*/

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class                     instance     title     tags mask     isfloating   monitor */
	{ "Gimp",                    NULL,        NULL,     0,            1,             -1 },
//  { "Plank",                   NULL,        NULL,     ~0,           1,             -1 },
	{ "Code",                    NULL,        NULL,     1 << 1,       0,             -1 },
	{ "Subl",                    NULL,        NULL,     1 << 1,       0,             -1 },
//	{ "Sublime_text",            NULL,        NULL,     1 << 1,       0,             -1 },
//	{ "Sublime_merge",           NULL,        NULL,     1 << 4,       0,             -1 },
	{ "Smerge",                  NULL,        NULL,     1 << 4,       0,             -1 },
	{ "Typora",                  NULL,        NULL,     1 << 1,       0,             -1 },
	{ "wps",                     NULL,        NULL,     1 << 2,       0,             -1 },
	{ "wpp",                     NULL,        NULL,     1 << 2,       0,             -1 },
	{ "^et",                     NULL,        NULL,     1 << 2,       0,             -1 },
	{ "obsidian",                NULL,        NULL,     1 << 3,       0,             -1 },
	{ "pcmanfm",                 NULL,        NULL,     1 << 7,       0,             -1 },
	{ "Surf",                     NULL,        NULL,     1 << 8,       0,             -1 },
	{ "firefox",                 NULL,        NULL,     1 << 8,       0,             -1 },
	{ "Chromium",                NULL,        NULL,     1 << 8,       0,             -1 },
	{ "Microsoft-edge",          NULL,        NULL,     1 << 8,       0,             -1 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

#include "gaplessgrid.c"
#include "fibonacci.c"
static const Layout layouts[] = {
	/* symbol     arrange function */
//	{ "[]=",      tile },    /* first entry is default */
//	{ "><>",      NULL },    /* no layout function means floating behavior */
//	{ "[M]",      monocle },
	{ "",        tile },    /* first entry is default */
	{ "",        NULL },    /* no layout function means floating behavior */
	{ "",        monocle },
 	{ "󰕰",        gaplessgrid },
 	{ "󰉪",        dwindle },
 	{ "󰉨",        spiral },
};

/* custom symbols for nr. of clients in monocle layout */
/* when clients >= LENGTH(monocles), uses the last element */
static const char *monocles[] = { "󰎥", "󰎨", "󰎫", "󰎲", "󰎯", "󰎴", "󰎷", "󰎺", "󰎽", "󰏀" };

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", NULL };
/* solarized dark */
// static const char *dmenusc[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nf", s_base0, "-nb", s_base03, "-sf", s_base0, "-sb", s_base02, NULL };
/* solarized light */
static const char *dmenusc[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nf", s_base00, "-nb", s_base3, "-sf", s_base00, "-sb", s_base2, NULL };
/* dracula */
// static const char *dmenusc[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nf", col_fg, "-nb", col_bg, "-sf", col_fg, "-sb", col_comment, NULL };

// static const char *termcmd[]  = { "st", NULL };
static const char *termcmd[]  = { "tabbed", "-o", "#002b36", "-O", "#839496", "-t", "#073642", "-T", "#93a1a1", "-c", "-r", "2", "st", "-w", "''", NULL };
static const char *termcmdalt[]  = { "tabbed", "-o", "#fdf6e3", "-O", "#657b83", "-t", "#eee8d5", "-T", "#586e75", "-c", "-r", "2", "st", "-w", "''", NULL };
//static const char *termcmd[]  = { "alacritty", NULL };
static const char *roficmd[] = { "rofi", "-show", "combi", NULL };
static const char *slockcmd[]  = { "slock", NULL };
static const char *flameshot[]  = { "flameshot", "gui", NULL };
static const char *sdcvcmd[]  = { "bash", "/home/wonux/.stardict/xsdcv.sh", NULL };
static const char *volup[]  = { "bash", "/home/wonux/.config/dwm/volume-up.sh", NULL };
static const char *voldown[]  = { "bash", "/home/wonux/.config/dwm/volume-down.sh", NULL };
static const char *volmute[]  = { "bash", "/home/wonux/.config/dwm/volume-mute.sh", NULL };
static const char *wallpaper[]  = { "bash", "/home/wonux/.config/dwm/wallpaper.sh", NULL };
/* scratchpad */
static const char scratchpadname[] = "scratchpad";
// static const char *scratchpadcmd[] = { "st", "-t", scratchpadname, "-g", "120x34", NULL };

static const Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
	{ MODKEY|ShiftMask,             XK_p,      spawn,          {.v = dmenusc } },
	{ MODKEY|ShiftMask,             XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY|ControlMask,           XK_Return, spawn,          {.v = termcmdalt } },
	{ MODKEY|Mod1Mask,              XK_Return, spawn,          SHCMD("tabbed alacritty --embed")},
	{ MODKEY,                       XK_w,      spawn,          SHCMD("tabbed -c -n Surf surf -Se")},
//	{ MODKEY,                       XK_a,      togglescratch,  {.v = scratchpadcmd } },
	{ MODKEY,                       XK_a,      togglescratch,  SHCMD("st -t scratchpad -g 120x34")},
	{ MODKEY,                       XK_r,      spawn,          {.v = roficmd } },
	{ MODKEY|ShiftMask,             XK_l,      spawn,          {.v = slockcmd } },
	{ Mod1Mask,                     XK_a,      spawn,          {.v = flameshot } },	
	{ MODKEY,                       XK_q,      spawn,          {.v = sdcvcmd } },
	{ MODKEY,                       XK_equal,  spawn,          {.v = volup } },
	{ MODKEY,                       XK_minus,  spawn,          {.v = voldown } },
	{ MODKEY|ShiftMask,             XK_m,      spawn,          {.v = volmute } },
	{ MODKEY|ShiftMask,             XK_w,      spawn,          {.v = wallpaper} },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_g,      setlayout,      {.v = &layouts[3]} },
	{ MODKEY|ShiftMask,             XK_t,      setlayout,      {.v = &layouts[4]} },
	{ MODKEY|ShiftMask|ControlMask, XK_t,      setlayout,      {.v = &layouts[5]} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_s,      togglesticky,   {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	{ MODKEY,                       XK_n,      togglealttag,   {0} },
	{ MODKEY,                       XK_o,      winview,        {0} },
	{ MODKEY,                       XK_z,      schemeToggle,   {0} },
	{ MODKEY|ShiftMask,             XK_z,      schemeCycle,    {0} },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
	{ MODKEY|ShiftMask,             XK_r,      quit,           {1} }, 
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button1,        zoom,           {0} },
	{ ClkStatusText,        0,              Button3,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

